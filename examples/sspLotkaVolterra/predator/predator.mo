model predator
	// Inspired from Lotka-Volterra equations
	parameter Real fedPredatorGrowth = 0.105;
	parameter Real predatorDeathRate = 0.36;
	input Real preys;
	output Real predators;
initial equation
	preys=1.2;
	predators=1.0;
equation
	der(predators) = der(time) * predators * ( fedPredatorGrowth * preys - predatorDeathRate );
end predator;
