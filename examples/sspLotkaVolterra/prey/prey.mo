model prey
	// Inspired from Lotka-Volterra equations
	parameter Real preyGrowth = 0.05;
	parameter Real preydation = 0.075;
	output Real preys;
	input Real predators;
initial equation
	preys=1.2;
	predators=1.0;
equation
	der(preys)= der(time) * preys * (preyGrowth - preydation * predators);
end prey;
