#!/bin/bash


# Creates if not exists the installation folder
if [ ! -d $HOME/.local ]; then
   mkdir $HOME/.local
fi

if [ ! -d $HOME/.local/bin ]; then
   mkdir $HOME/.local/bin
fi

# Install required libraries
apt install libzip-dev libpugixml-dev

# Copies the executable there
cp ./releases/1.05_linux_x86_64/cosimini $HOME/.local/bin/cosimini

# Grant permissions
chmod u+x $HOME/.local/bin/cosimini

# Adds permanently the location to the path
echo "export PATH=\$PATH:\$HOME/.local/bin" >> ~/.bashrc
