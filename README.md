# Cosimini

Cosimini, cosimulation engine by Capgemini, is a model orchestrator enabling cosimulation of FMUs following 2.0 standard

## Installation

This process has been tested on Ubuntu 22.04.3 LTS

To install Cosimini, run the installation script:
```
bash install.sh
```

You can then check the installation:
```
cosimini --version
```

## Run

You can use Cosimini using scripted scenarios (see examples in test folder)

```
cosimini script.cos
```

Or you can run it from CLI by just starting cosimini.

## License

This software belongs to Capgemini Engineering, and is distributed by Capgemini. Please refer to LICENCE.txt file to get more information about Cosimini use and conditions.


