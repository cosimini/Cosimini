#!/bin/bash

# script is supposed to be executed in main directory

# remove target directory if it exists
if [ -d csv_writer ]; then
  rm -rf csv_writer 
fi &&

# remove target FMU if it exists
if [ -f csv_writer.fmu ]; then
    rm csv_writer.fmu 
fi &&

# create subdir and change into it
mkdir -p csv_writer &&
cd csv_writer &&

# create binary dir for Linux
mkdir -p binaries/linux64 &&

# copy shared library, we expect it to be already renamed correctly
cp ../binaries/linux64/csv_writer.so binaries/linux64/csv_writer.so &&
cp ../modelDescription.xml . &&

# create zip archive
zip -r ../csv_writer.zip binaries modelDescription.xml
cd .. && 
mv csv_writer.zip csv_writer.fmu &&
rm -r csv_writer
echo "Created csv_writer.fmu"

# change working directory back to original dir
#cd -

