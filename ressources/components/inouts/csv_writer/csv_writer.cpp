#include "gen/csv_writer.h"
#include <iostream>
#include <fstream>      // std::ifstream
#include <sstream>      // istringstream
#include <string>
#include <vector>

void csv_writer::setResourceLocation(const char* fmiResourceLocation){
}

fmi2Status csv_writer::init(){
    cumulatedTime=0.0;
    std::cout << "Writer filename " << filename << std::endl;
    ofs.open(filename);
    ofs << cumulatedTime << ", " << x << std::endl;
    return fmi2OK;
}

fmi2Status csv_writer::doStep(double step)
{
    cumulatedTime+=step;
    
		ofs << cumulatedTime << ", " << x << std::endl;
    return fmi2OK;
}

fmi2Status csv_writer::terminate(){
		ofs.close();
    return fmi2OK;
}

fmi2Status csv_writer::reset(){
    return fmi2OK;
}

double csv_writer::getReal(int fmi2ValueReference){
    return realVariables[fmi2ValueReference].getter(this);
}

void csv_writer::setReal(int fmi2ValueReference, double value){
    realVariables[fmi2ValueReference].setter(this, value);
}

int csv_writer::getInteger(int fmi2ValueReference){
    return integerVariables[fmi2ValueReference].getter(this);
}

void csv_writer::setInteger(int fmi2ValueReference, int value){
    integerVariables[fmi2ValueReference].setter(this, value);
}

bool csv_writer::getBoolean(int fmi2ValueReference){
    return booleanVariables[fmi2ValueReference].getter(this);
}

void csv_writer::setBoolean(int fmi2ValueReference, bool value){
    booleanVariables[fmi2ValueReference].setter(this, value);
}

const char* csv_writer::getString(int fmi2ValueReference){
    return stringVariables[fmi2ValueReference].getter(this);
}

void csv_writer::setString(int fmi2ValueReference, const char* value){
    std::cout << "Entering setter" << std::endl;
    stringVariables[fmi2ValueReference].setter(this, value);
    std::cout << "Exiting setter" << std::endl;
}
