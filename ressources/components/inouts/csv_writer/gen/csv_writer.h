#ifndef DEF_CSV_WRITER_H
#define DEF_CSV_WRITER_H
#include <gen/Simulation.h>
#include <gen/callbacks.h>
#include <fstream>      // std::ifstream
#include <functional>
#include <iostream>
#include <string.h>
#include <vector>

class csv_writer;

typedef enum {
    exact,
    approx,
    calculated
} initial;

typedef enum {
    constant,
    fixed,
    tunable,
    discrete,
    continuous
} variability;

typedef enum {
    parameter,
    calculatedParamater,
    local,
    independent,
    input,
    output
} causality;

typedef enum {
    Real,
    Integer,
    Boolean,
    String
} type;

struct RealVariable{
    const char* name;
    std::function<double (csv_writer* csv_writer)> getter;
    std::function<void (csv_writer* csv_writer, double value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct IntegerVariable{
    const char* name;
    std::function<int (csv_writer* csv_writer)> getter;
    std::function<void (csv_writer* csv_writer, int value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct BooleanVariable{
    const char* name;
    std::function<bool (csv_writer* csv_writer)> getter;
    std::function<void (csv_writer* csv_writer, bool value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct StringVariable{
    const char* name;
    std::function<const char* (csv_writer* csv_writer)> getter;
    std::function<void (csv_writer* csv_writer, const char* value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

class csv_writer : public Simulation{
public:
    // Declare here inputs, outputs and parameters
    double x=0.0;
    char filename[100] = "out.csv";
    void setResourceLocation(const char* fmiResourceLocation);
    fmi2Status init();
    fmi2Status doStep(double step);
    fmi2Status terminate();
    fmi2Status reset();
    double getReal(int fmi2ValueReference);
    void setReal(int fmi2ValueReference, double value);
    int getInteger(int fmi2ValueReference);
    void setInteger(int fmi2ValueReference, int value);
    bool getBoolean(int fmi2ValueReference);
    void setBoolean(int fmi2ValueReference, bool value);
    const char* getString(int fmi2ValueReference);
    void setString(int fmi2ValueReference, const char* value);
    using Simulation::Simulation;

    double cumulatedTime=0.0;
    int currentIndex=0;
    
    std::ofstream ofs;
    void setOutputFile(const char * value)
    {
      strcpy(filename,value);
    }
};

static RealVariable realVariables[] ={
    {"x",
        [] (csv_writer* csv_writer) {return csv_writer->x;},
        [] (csv_writer* csv_writer, double value) {csv_writer->x = value;},
        [] () {return exact;},
        [] () {return continuous;},
        [] () {return input;},
        [] () {return Real;}
    }
};

static IntegerVariable integerVariables[] ={

};

static BooleanVariable booleanVariables[] ={

};

static StringVariable stringVariables[] ={
    {"filename",
        [] (csv_writer* csv_writer) {return csv_writer->filename;},
        [] (csv_writer* csv_writer, const char* value) {csv_writer->setOutputFile(value);},
        [] () {return exact;},
        [] () {return fixed;},
        [] () {return parameter;},
        [] () {return String;}
    }
};

#endif DEF_CSV_writer_H
