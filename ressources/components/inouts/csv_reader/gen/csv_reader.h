#ifndef DEF_CSV_READER_H
#define DEF_CSV_READER_H
#include <gen/Simulation.h>
#include <gen/callbacks.h>
#include <functional>
#include <iostream>
#include <string.h>
#include <vector>

class csv_reader;

typedef enum {
    exact,
    approx,
    calculated
} initial;

typedef enum {
    constant,
    fixed,
    tunable,
    discrete,
    continuous
} variability;

typedef enum {
    parameter,
    calculatedParamater,
    local,
    independent,
    input,
    output
} causality;

typedef enum {
    Real,
    Integer,
    Boolean,
    String
} type;

struct RealVariable{
    const char* name;
    std::function<double (csv_reader* csv_reader)> getter;
    std::function<void (csv_reader* csv_reader, double value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct IntegerVariable{
    const char* name;
    std::function<int (csv_reader* csv_reader)> getter;
    std::function<void (csv_reader* csv_reader, int value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct BooleanVariable{
    const char* name;
    std::function<bool (csv_reader* csv_reader)> getter;
    std::function<void (csv_reader* csv_reader, bool value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct StringVariable{
    const char* name;
    std::function<const char* (csv_reader* csv_reader)> getter;
    std::function<void (csv_reader* csv_reader, const char* value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

class csv_reader : public Simulation{
public:
    // Declare here inputs, outputs and parameters
    const char* instanceName;
    double y=0.0;
    const char * _fmiResourceLocation = "location";
    void setResourceLocation(const char* fmiResourceLocation);
    fmi2Status init();
    fmi2Status doStep(double step);
    fmi2Status terminate();
    fmi2Status reset();
    double getReal(int fmi2ValueReference);
    void setReal(int fmi2ValueReference, double value);
    int getInteger(int fmi2ValueReference);
    void setInteger(int fmi2ValueReference, int value);
    bool getBoolean(int fmi2ValueReference);
    void setBoolean(int fmi2ValueReference, bool value);
    const char* getString(int fmi2ValueReference);
    void setString(int fmi2ValueReference, const char* value);
    using Simulation::Simulation;

    double cumulatedTime;
    int currentIndex=0;
    
    std::vector<double> timeValues;
    std::vector<double> signalValues;
    char inputFile[100] = "data.csv";
    
    void setInputFile(const char * value)
    {
      strcpy(inputFile,value);
    }
};

static RealVariable realVariables[] ={
    {"y",
        [] (csv_reader* csv_reader) {return csv_reader->y;},
        [] (csv_reader* csv_reader, double value) {csv_reader->y = value;},
        [] () {return exact;},
        [] () {return continuous;},
        [] () {return output;},
        [] () {return Real;}
    }
};

static IntegerVariable integerVariables[] ={

};

static BooleanVariable booleanVariables[] ={

};

static StringVariable stringVariables[] ={
    {"inputFile",
        [] (csv_reader* csv_reader) {return csv_reader->inputFile;},
        [] (csv_reader* csv_reader, const char* value) { std::cout << "set variable inputFile as " << value << std::endl; csv_reader->setInputFile(value);},
        [] () {return exact;},
        [] () {return fixed;},
        [] () {return parameter;},
        [] () {return String;}
    }
};

#endif //DEF_CSV_READER_H
