#ifndef SKELETON_H
#define SKELETON_H

#include "fmi/fmi2Functions.h"
#include "gen/csv_reader.h"

fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID,
    fmi2String fmuResourceLocation, const fmi2CallbackFunctions* callbacks,
    fmi2Boolean visible, fmi2Boolean loggingOn);
fmi2Real fmi2GetReal(fmi2ValueReference reference);
fmi2Integer fmi2GetInteger(fmi2ValueReference reference);
fmi2Integer fmi2GetBoolean(fmi2ValueReference reference);
fmi2String fmi2GetString(fmi2ValueReference reference);
void fmi2SetReal(fmi2ValueReference reference, fmi2Real value);
void fmi2SetInteger(fmi2ValueReference reference, fmi2Integer value);
void fmi2SetBoolean(fmi2ValueReference reference, fmi2Boolean value);
void fmi2SetString(fmi2ValueReference reference, fmi2String value);


#endif // SKELETON_H
