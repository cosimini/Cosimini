#ifndef FMISIMULATION_H
#define FMISIMULATION_H
#include "fmi/fmi2Functions.h"
#include <gen/callbacks.h>

class Simulation
{

    public:
        const char* id;
        fmi2Component* component;

        virtual void setResourceLocation(const char* fmiResourceLocation) = 0;
        virtual fmi2Status init() = 0;
        virtual fmi2Status doStep(double step) = 0;
        virtual fmi2Status terminate() = 0;
        virtual fmi2Status reset() = 0;
        virtual double getReal(int fmi2ValueReference) = 0;
        virtual void setReal(int fmi2ValueReference, double value) = 0;
        virtual int getInteger(int fmi2ValueReference) = 0;
        virtual void setInteger(int fmi2ValueReference, int value) = 0;
        virtual bool getBoolean(int fmi2ValueReference) = 0;
        virtual void setBoolean(int fmi2ValueReference, bool value) = 0;
        virtual const char* getString(int fmi2ValueReference) = 0;
        virtual void setString(int fmi2ValueReference, const char* value) = 0;

        Simulation(const char* instanceId){
            id = instanceId;
        }

        void setComponent(fmi2Component* component){
            this->component = component;
        }

        void* operator new(size_t size){
            return fmiAlloc(size);
        }

        void operator delete(void * p){
            fmiFree(p);
        }

        void logInfo(const char* text){
            log(component, id, fmi2OK, "INFO", text, nullptr);
        }

        void logError(const char* text){
            log(component, id, fmi2Error, "ERROR", text, nullptr);
        }
};
#endif // FMISIMULATION_H
