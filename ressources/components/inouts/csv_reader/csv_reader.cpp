#include "gen/csv_reader.h"
#include <iostream>
#include <fstream>      // std::ifstream
#include <sstream>      // istringstream
#include <string>
#include <vector>

void csv_reader::setResourceLocation(const char* fmiResourceLocation){
  _fmiResourceLocation = fmiResourceLocation;
}

fmi2Status csv_reader::init(){
    std::cout << "The parameter is " << inputFile << std::endl;
    cumulatedTime=0.0;
    y=0.0;
    std::ifstream ifs (inputFile, std::ifstream::in);
    if (ifs.is_open())
    {
      std::cout << "file " << inputFile << " found and opened" << std::endl;
      std::string line;
      int lineNumber=0;
      while(std::getline(ifs, line)) //read data from file object and put it into string.
      {
      	std::string stime = line.substr(0,line.find(","));
        std::istringstream in(stime);
        double time;
        in>>time; // cast
        line.erase(0,line.find(",")+1); // remove the part
        std::istringstream issVal(line);
        double value;
        issVal>>value;
        
        timeValues.push_back(time);
        signalValues.push_back(value);
      }
    }
    else
    {
      std::cout << "File " << inputFile << " not opened" << std::endl;
    }
    return fmi2OK;
}

fmi2Status csv_reader::doStep(double step)
{
    cumulatedTime+=step;
    
		// Move index forward as long as needed
		while(currentIndex+1 < timeValues.size() && timeValues[currentIndex+1] <= cumulatedTime)
		{
			currentIndex++;
		}
    y=signalValues[currentIndex];
    return fmi2OK;
}

fmi2Status csv_reader::terminate(){
    return fmi2OK;
}

fmi2Status csv_reader::reset(){
    return fmi2OK;
}

double csv_reader::getReal(int fmi2ValueReference){
    return realVariables[fmi2ValueReference].getter(this);
}

void csv_reader::setReal(int fmi2ValueReference, double value){
    realVariables[fmi2ValueReference].setter(this, value);
}

int csv_reader::getInteger(int fmi2ValueReference){
    return integerVariables[fmi2ValueReference].getter(this);
}

void csv_reader::setInteger(int fmi2ValueReference, int value){
    integerVariables[fmi2ValueReference].setter(this, value);
}

bool csv_reader::getBoolean(int fmi2ValueReference){
    return booleanVariables[fmi2ValueReference].getter(this);
}

void csv_reader::setBoolean(int fmi2ValueReference, bool value){
    booleanVariables[fmi2ValueReference].setter(this, value);
}

const char* csv_reader::getString(int fmi2ValueReference){
		std::cout << "[csv_reader fmu] called getString" << std::endl;
    return stringVariables[fmi2ValueReference].getter(this);
}

void csv_reader::setString(int fmi2ValueReference, const char* value){
		std::cout << "[csv_reader fmu] called setString" << std::endl;
    stringVariables[fmi2ValueReference].setter(this, value);
    std::cout << "The parameter is " << inputFile << " and value " << value <<std::endl;
}
