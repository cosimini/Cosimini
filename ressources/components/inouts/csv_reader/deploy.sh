#!/bin/bash

# script is supposed to be executed in main directory

# remove target directory if it exists
if [ -d csv_reader ]; then
  rm -rf csv_reader 
fi &&

# remove target FMU if it exists
if [ -f csv_reader.fmu ]; then
    rm csv_reader.fmu 
fi &&

# create subdir and change into it
mkdir -p csv_reader &&
cd csv_reader &&

# create binary dir for Linux
mkdir -p binaries/linux64 &&

# copy shared library, we expect it to be already renamed correctly
cp ../binaries/linux64/csv_reader.so binaries/linux64/csv_reader.so &&
cp ../modelDescription.xml . &&

# create zip archive
zip -r ../csv_reader.zip binaries modelDescription.xml
cd .. && 
mv csv_reader.zip csv_reader.fmu &&
rm -r csv_reader
echo "Created csv_reader.fmu"

# change working directory back to original dir
#cd -

