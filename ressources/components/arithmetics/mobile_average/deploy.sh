#!/bin/bash

# script is supposed to be executed in main directory

# remove target directory if it exists
if [ -d mobile_average ]; then
  rm -rf mobile_average 
fi &&

# remove target FMU if it exists
if [ -f mobile_average.fmu ]; then
    rm mobile_average.fmu 
fi &&

# create subdir and change into it
mkdir -p mobile_average &&
cd mobile_average &&

# create binary dir for Linux
mkdir -p binaries/linux64 &&

# copy shared library, we expect it to be already renamed correctly
cp ../binaries/linux64/mobile_average.so binaries/linux64/mobile_average.so &&
cp ../modelDescription.xml . &&

# create zip archive
zip -r ../mobile_average.zip binaries modelDescription.xml
cd .. && 
mv mobile_average.zip mobile_average.fmu &&
rm -r mobile_average
echo "Created mobile_average.fmu"

# change working directory back to original dir
#cd -

