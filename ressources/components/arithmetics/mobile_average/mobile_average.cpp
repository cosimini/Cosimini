#include "gen/mobile_average.h"
#include <iostream>
#include <fstream>      // std::ifstream
#include <sstream>      // istringstream
#include <string>
#include <vector>

void mobile_average::setResourceLocation(const char* fmiResourceLocation){
  _fmiResourceLocation = fmiResourceLocation;
}

fmi2Status mobile_average::init(){
    return fmi2OK;
}

fmi2Status mobile_average::doStep(double step)
{
    signalValues.push_front(x);
    if (signalValues.size() > n)
      signalValues.pop_back();

    y = 0.0;
    for (double elem : signalValues)
      y+=elem;
    y = y / n;
    return fmi2OK;
}

fmi2Status mobile_average::terminate(){
    return fmi2OK;
}

fmi2Status mobile_average::reset(){
    return fmi2OK;
}

double mobile_average::getReal(int fmi2ValueReference){
    return realVariables[fmi2ValueReference].getter(this);
}

void mobile_average::setReal(int fmi2ValueReference, double value){
    realVariables[fmi2ValueReference].setter(this, value);
}

int mobile_average::getInteger(int fmi2ValueReference){
    return integerVariables[fmi2ValueReference].getter(this);
}

void mobile_average::setInteger(int fmi2ValueReference, int value){
    integerVariables[fmi2ValueReference].setter(this, value);
}

bool mobile_average::getBoolean(int fmi2ValueReference){
    return booleanVariables[fmi2ValueReference].getter(this);
}

void mobile_average::setBoolean(int fmi2ValueReference, bool value){
    booleanVariables[fmi2ValueReference].setter(this, value);
}

const char* mobile_average::getString(int fmi2ValueReference){
    return stringVariables[fmi2ValueReference].getter(this);
}

void mobile_average::setString(int fmi2ValueReference, const char* value){
    stringVariables[fmi2ValueReference].setter(this, value);
}
