#ifndef DEF_mobile_average_H
#define DEF_mobile_average_H
#include <gen/Simulation.h>
#include <gen/callbacks.h>
#include <functional>
#include <iostream>
#include <string.h>
#include <list>

class mobile_average;

typedef enum {
    exact,
    approx,
    calculated
} initial;

typedef enum {
    constant,
    fixed,
    tunable,
    discrete,
    continuous
} variability;

typedef enum {
    parameter,
    calculatedParamater,
    local,
    independent,
    input,
    output
} causality;

typedef enum {
    Real,
    Integer,
    Boolean,
    String
} type;

struct RealVariable{
    const char* name;
    std::function<double (mobile_average* mobile_average)> getter;
    std::function<void (mobile_average* mobile_average, double value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct IntegerVariable{
    const char* name;
    std::function<int (mobile_average* mobile_average)> getter;
    std::function<void (mobile_average* mobile_average, int value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct BooleanVariable{
    const char* name;
    std::function<bool (mobile_average* mobile_average)> getter;
    std::function<void (mobile_average* mobile_average, bool value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

struct StringVariable{
    const char* name;
    std::function<const char* (mobile_average* mobile_average)> getter;
    std::function<void (mobile_average* mobile_average, const char* value)> setter;
    std::function<initial ()> initial;
    std::function<variability ()> variability;
    std::function<causality ()> causality;
    std::function<type ()> type;
};

class mobile_average : public Simulation{
public:
    // Declare here inputs, outputs and parameters
    double x=0.0, y=0.0;
    int n=10;
    const char * _fmiResourceLocation = "location";
    void setResourceLocation(const char* fmiResourceLocation);
    fmi2Status init();
    fmi2Status doStep(double step);
    fmi2Status terminate();
    fmi2Status reset();
    double getReal(int fmi2ValueReference);
    void setReal(int fmi2ValueReference, double value);
    int getInteger(int fmi2ValueReference);
    void setInteger(int fmi2ValueReference, int value);
    bool getBoolean(int fmi2ValueReference);
    void setBoolean(int fmi2ValueReference, bool value);
    const char* getString(int fmi2ValueReference);
    void setString(int fmi2ValueReference, const char* value);
    using Simulation::Simulation;
    
    std::list<double> signalValues;
    
};

static RealVariable realVariables[] ={
    {"y",
        [] (mobile_average* mobile_average) {return mobile_average->y;},
        [] (mobile_average* mobile_average, double value) {mobile_average->y = value;},
        [] () {return exact;},
        [] () {return continuous;},
        [] () {return output;},
        [] () {return Real;}
    },
    {"x",
        [] (mobile_average* mobile_average) {return mobile_average->x;},
        [] (mobile_average* mobile_average, double value) {mobile_average->x = value;},
        [] () {return exact;},
        [] () {return continuous;},
        [] () {return input;},
        [] () {return Real;}
    }
};

static IntegerVariable integerVariables[] ={
    {"n",
        [] (mobile_average* mobile_average) {return mobile_average->n;},
        [] (mobile_average* mobile_average, int value) {mobile_average->n = value;},
        [] () {return exact;},
        [] () {return fixed;},
        [] () {return parameter;},
        [] () {return Integer;}
    }
};

static BooleanVariable booleanVariables[] ={

};

static StringVariable stringVariables[] ={

};

#endif //DEF_mobile_average_H
