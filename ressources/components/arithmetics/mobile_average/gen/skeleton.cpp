#include "gen/skeleton.h"
#include "gen/mobile_average.h"

mobile_average *simulation;
fmi2Component *component;

const char* fmi2GetTypesPlatform() {
    return "default";
}

const char* fmi2GetVersion() {
    return "2.0";
}

fmi2Status fmi2SetDebugLogging(fmi2Component _component, fmi2Boolean loggingOn, size_t numberOfCategories,
    const fmi2String categories[]) {
    return fmi2Error;
}

fmi2Component fmi2Instantiate(fmi2String instanceName, fmi2Type fmuType, fmi2String fmuGUID,
    fmi2String fmuResourceLocation, const fmi2CallbackFunctions* callbacks,
    fmi2Boolean visible, fmi2Boolean loggingOn) {
    setFunctions(callbacks);
    simulation = new mobile_average(instanceName);
    simulation->setResourceLocation(fmuResourceLocation);
    component = (fmi2Component*) fmiAlloc(sizeof(fmi2Component));
    simulation->setComponent(component);
    return component;
}


fmi2Status fmi2SetupExperiment(fmi2Component _component, fmi2Boolean toleranceDefined, fmi2Real tolerance,
    fmi2Real startTime, fmi2Boolean stopTimeDefined, fmi2Real stopTime) {
    return fmi2OK;
}

fmi2Status fmi2EnterInitializationMode(fmi2Component _component) {
    return simulation->init();
}

fmi2Status fmi2ExitInitializationMode(fmi2Component _component) {
    return fmi2OK;
}

fmi2Status fmi2Terminate(fmi2Component _component) {
    return simulation->terminate();
}

void fmi2FreeInstance(fmi2Component _component) {
    delete simulation;
    simulation = nullptr;
    fmiFree(component);
}

fmi2Status fmi2Reset(fmi2Component _component) {
    return simulation->reset();
}

fmi2Status fmi2GetReal(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    fmi2Real values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++)
            values[i] = fmi2GetReal(valueReference[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

fmi2Real fmi2GetReal(fmi2ValueReference reference) {
    return simulation->getReal(reference);
}

fmi2Status fmi2GetInteger(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    fmi2Integer values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++)
            values[i] = fmi2GetInteger(valueReference[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

fmi2Integer fmi2GetInteger(fmi2ValueReference reference) {
    return simulation->getInteger(reference);
}

fmi2Status fmi2GetBoolean(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    fmi2Boolean values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++)
            values[i] = fmi2GetBoolean(valueReference[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

fmi2Integer fmi2GetBoolean(fmi2ValueReference reference) {
    return simulation->getBoolean(reference);
}

fmi2Status fmi2GetString(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    fmi2String values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++)
            values[i] = fmi2GetString(valueReference[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

fmi2String fmi2GetString(fmi2ValueReference reference) {
    return simulation->getString(reference);
}

fmi2Status fmi2SetReal(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    const fmi2Real values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++) fmi2SetReal(valueReference[i], values[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

void fmi2SetReal(fmi2ValueReference reference, fmi2Real value){
    simulation->setReal(reference, value);
}

fmi2Status fmi2SetInteger(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    const fmi2Integer values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++) fmi2SetInteger(valueReference[i], values[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

void fmi2SetInteger(fmi2ValueReference reference, fmi2Integer value){
    simulation->setInteger(reference, value);
}

fmi2Status fmi2SetBoolean(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    const fmi2Boolean values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++) fmi2SetBoolean(valueReference[i], values[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

void fmi2SetBoolean(fmi2ValueReference reference, fmi2Boolean value){
    simulation->setBoolean(reference, value);
}

fmi2Status fmi2SetString(fmi2Component _component, const fmi2ValueReference valueReference[], size_t numberOfValues,
    const fmi2String values[]) {
    try {
        for (size_t i = 0; i < numberOfValues; i++) fmi2SetString(valueReference[i], values[i]);
        return fmi2OK;
    } catch (...) {
        return fmi2Fatal;
    }
}

void fmi2SetString(fmi2ValueReference reference, const char* value){
    simulation->setString(reference, value);
}

fmi2Status fmi2GetFMUstate(fmi2Component _component, fmi2FMUstate* state) {
    return fmi2Error;
}

fmi2Status fmi2SetFMUstate(fmi2Component _component, fmi2FMUstate state) {
    return fmi2Error;
}

fmi2Status fmi2FreeFMUstate(fmi2Component _component, fmi2FMUstate* state) {
    return fmi2Error;
}

fmi2Status fmi2SerializedFMUstateSize(fmi2Component _component, fmi2FMUstate, size_t* stateSize) {
    return fmi2Error;
}

fmi2Status fmi2SerializeFMUstate(fmi2Component _component, fmi2FMUstate state, fmi2Byte serializedState[],
    size_t serializedStateSize) {
    return fmi2Error;
}

fmi2Status fmi2DeSerializeFMUstate(fmi2Component _component, const fmi2Byte serializedState, size_t size,
    fmi2FMUstate* state) {
    return fmi2Error;
}

fmi2Status fmi2GetDirectionalDerivative(fmi2Component _component, const fmi2ValueReference unknownValueReferences[],
    size_t numberOfUnknowns, const fmi2ValueReference knownValueReferences[],
    fmi2Integer numberOfKnowns, fmi2Real knownDifferential[],
    fmi2Real unknownDifferential[]) {
    return fmi2Error;
}

fmi2Status fmi2SetRealInputDerivatives(fmi2Component _component, const fmi2ValueReference valueReferences[],
    size_t numberOfValueReferences, fmi2Integer orders[], const fmi2Real values[]) {
    return fmi2Error;
}

fmi2Status fmi2GetRealOutputDerivatives(fmi2Component _component, const fmi2ValueReference valueReference[],
    size_t numberOfValues, const fmi2Integer order[], fmi2Real values[]) {
    return fmi2Error;
}

fmi2Status fmi2DoStep(fmi2Component _component, fmi2Real currentComunicationTime, fmi2Real stepSize,
    fmi2Boolean noSetFmuFmuStatePriorToCurrentPoint) {
    return simulation->doStep(stepSize);
}

fmi2Status fmi2CancelStep(fmi2Component _component) {
    return fmi2Warning;
}

fmi2Status fmi2GetStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Status* status) {
    return fmi2Error;
}

fmi2Status fmi2GetRealStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Real* value) {
    return fmi2Error;
}

fmi2Status fmi2GetIntegerStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Integer* value) {
    return fmi2Error;
}

fmi2Status fmi2GetBooleanStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2Boolean* value) {
    return fmi2Error;
}

fmi2Status fmi2GetStringStatus(fmi2Component _component, const fmi2StatusKind kind, fmi2String* value) {
    return fmi2Error;
}
